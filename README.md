# CS378_A2

---Report---

The screenshots are all in the root folder. The video is stored inside the video folder. The source code is in Source/CS378_A2.

//Idle: If velocity IsNearlyZero(), then we enter the IDLE state.

//Movement: The movement speed between crouching, walking, and running is performed in our ApplyMovement() and ApplyStrafe() method.

Running: default velocity
Walking: 1/2 velocity
Crouch: 1/5th velocity

The state of running, walking, and crouching states are updated separately from the FSM of UpdateActionState (idle, jump, interact).
There is an UpdateCrouchState() and an UpdateRunningState() function.


//Camera/Turn: The yaw is implemented in ApplyYaw(), where we set the world rotation of the spring arm component to the current yaw rotation + the input of the Mouse.X

The pitch is implemented in ApplyPitch(), where we also set the world rotation of the spring arm component to the current pitch rotation + the input of the Mouse.Y,
but under a clamp to prevent over-rotation of the pitch axis.

The character itself does not rotate with the spring arm initially. When the player right clicks, the character rotation snaps to the spring arm’s rotation.
There is a snapping state boolean bSnappingCharacter that is set to true on key press and false otherwise, so that when the player holds right click,
the character will continue rotate with the spring arm until button release. 


//Jump: We set the JumpMaxCount variable of our A2Character to be 2 to allow for the double jumping feature.


//Crouch: BeginCrouch() updates the crouching state and calls crouch() and EndCrouch() to NOTCROUCH and calls the UnCrouch() function. 
The crouching state is updated separately  from the UpdateAction() because it is possible to be in both the crouch and moving state at the same time.
As stated in the //Movement section, this logic also applied to the running feature. The prevention of jumping and crouching is done inside CanPerformAction().


//Interactables: When the player hits left click, BeginInteraction() shoots a raycast of 250 units long. The function used is LinetraceSingleByObject(),
targeting WorldDynamic objects and ignoring the player itself.

-All interactables are spawned in when the play begins.

-The relevant screenshot is “Begin Interaction.png”


    ~/Pick up & Pickupable object: 

    On the A2Character , there is a USceneComponent called Pickupable Attach Point that is attached to the root component. 

    If the raycast hits an AA2Pickupable object, that pickupable will be attached to the Pickupable Attach Point. Physics of the pickupable will be temporarily disabled. 

    -The pickupable will be attached to the player for as long as the the player holds the left click. 

    -Releasing the left click will trigger DropObject(), which detaches the object from the player and re-enables physics for the object.

    -Picking up an object does NOT switch the state, as players must be able to move and jump with the object. The player however, cannot pickup another
     object while holding a pickupable object. This is assured via a boolean variable.

    ~/Pull & Lever + Responder:

    If the raycast hits an AA2LeverActor object and the player is NOT crouching, then EndLeverInteraction would be called after 3 seconds on a timer via the SetTimer() function.
    The player would also enter the INTERACT state. 

    -EndLeverInteraction broadcasts on a Multicast delegate, which A2ReponseActor listens for. A2ResponseActor then destroys itself. The state would also be updated to IDLE here.
    -The pointer to the lever of which the response actor will respond to is set per instance as a UPROPERTY.


//Level Transition:
-The relevant screenshot is “Level Transition Volume.png”

If the player collides with the Level Transition Volume via the OnOverlap() function, then OpenLevel will be called to swap to the other level that the player is not in.
The level transition visual is represented by a simple area of light on the ground.


The FSM for animation is the same one demonstrated by Professor Abraham on lab3.
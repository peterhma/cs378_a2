// Fill out your copyright notice in the Description page of Project Settings.

#include "A2ResponseActor.h"
#include "A2LeverActor.h"

// Sets default values
AA2ResponseActor::AA2ResponseActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	RootComponent = MeshComponent;
}

// Called when the game starts or when spawned
void AA2ResponseActor::BeginPlay()
{
	Super::BeginPlay();

	if (A2LeverActor)
	{
		GEngine->AddOnScreenDebugMessage(-1, 1.0, FColor::Blue, FString::Printf(TEXT("Receive signal")));
		A2LeverActor->OnLeverDelegate.AddDynamic(this, &AA2ResponseActor::RespondToLever);
	}
}

void AA2ResponseActor::RespondToLever()
{
	GEngine->AddOnScreenDebugMessage(-1, 1.0, FColor::Blue, FString::Printf(TEXT("Responding to Lever Delegate")));
	this->Destroy();
}

// Called every frame
void AA2ResponseActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Fill out your copyright notice in the Description page of Project Settings.

#include "A2Character.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/Actor.h"
#include "Camera/CameraComponent.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetSystemLibrary.h"
#include "A2Pickupable.h"
#include "GameFramework/Controller.h"
#include "A2PlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "Components/SceneComponent.h"
#include "A2LeverActor.h"

// Sets default values
AA2Character::AA2Character()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// AutoPossessPlayer = EAutoReceiveInput::Player0;
	// bUseControllerRotationYaw = false;
	SpringArmComp = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComponent"));
	SpringArmComp->SetupAttachment(RootComponent);
	SpringArmComp->SetUsingAbsoluteRotation(true);
	SpringArmComp->bUsePawnControlRotation = false;
	SpringArmComp->TargetArmLength = 400.0f;
	SpringArmComp->SetWorldRotation(FRotator(-30.0f, GetControlRotation().Yaw, GetControlRotation().Roll));

	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComp->SetupAttachment(SpringArmComp, USpringArmComponent::SocketName);

	PickupableAttachPoint = CreateDefaultSubobject<USceneComponent>(TEXT("Pickupable Attach Point"));
	PickupableAttachPoint->SetupAttachment(RootComponent);

	holdingObject = false;

	bSnappingCharacter = false;

	JumpMaxCount = 2;
}

void AA2Character::ApplyMovement(float value)
{
	if (IsCrouching == ECharacterCrouchStateEnum::CROUCH)
	{
		AddMovementInput(GetActorForwardVector(), value / 5);
	}
	else
	{
		if (IsRunning == ECharacterRunningStateEnum::RUN)
		{
			AddMovementInput(GetActorForwardVector(), value);
		}
		else
		{
			AddMovementInput(GetActorForwardVector(), value / 2);
		}
	}
}

void AA2Character::ApplyStrafe(float value)
{
	if (IsCrouching == ECharacterCrouchStateEnum::CROUCH)
	{
		AddMovementInput(GetActorRightVector(), value / 5);
	}
	else
	{
		if (IsRunning == ECharacterRunningStateEnum::RUN)
		{
			AddMovementInput(GetActorRightVector(), value);
		}
		else
		{
			AddMovementInput(GetActorRightVector(), value / 2);
		}
	}
}

void AA2Character::ApplyPitch(float value)
{
	FRotator Rotation = SpringArmComp->GetComponentRotation();
	// Clamp pitch to avoid gimbal lock
	Rotation.Pitch = FMath::Clamp(Rotation.Pitch + value, -89.0f, 89.0f);

	if (value)
	{
		SpringArmComp->SetWorldRotation(Rotation);
	}
}

void AA2Character::ApplyYaw(float value)
{
	if (value)
	{
		FRotator Rotation = SpringArmComp->GetComponentRotation();
		Rotation.Yaw += value;
		Rotation.Roll = 0;
		SpringArmComp->SetWorldRotation(Rotation);
	}
}

void AA2Character::SnapCharToRotation()
{
	Controller->SetControlRotation(FRotator(0.0f, SpringArmComp->GetRelativeRotation().Yaw, 0.0f));
}

void AA2Character::StartSnappingRotation()
{
	bSnappingCharacter = true;
}

void AA2Character::StopSnappingRotation()
{
	bSnappingCharacter = false;
}

bool AA2Character::CanPerformAction(ECharacterActionStateEnum updatedAction)
{
	if (IsCrouching == ECharacterCrouchStateEnum::CROUCH && updatedAction == ECharacterActionStateEnum::JUMP)
	{
		return false;
	}
	switch (CurrentActionState)
	{
	case ECharacterActionStateEnum::IDLE:
		return true;
		break;
	case ECharacterActionStateEnum::MOVE:
		if (updatedAction == ECharacterActionStateEnum::INTERACT)
		{
			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, TEXT("You can't interact while moving"));
			return false;
		}
		return true;

		break;
	case ECharacterActionStateEnum::JUMP:
		if (updatedAction == ECharacterActionStateEnum::INTERACT || updatedAction == ECharacterActionStateEnum::MOVE || IsCrouching == ECharacterCrouchStateEnum::CROUCH)
		{
			return false;
		}
		return true;
		break;
	case ECharacterActionStateEnum::INTERACT:
		return false;
		break;
	}
	return false;
}

bool AA2Character::UpdateAction(ECharacterActionStateEnum newAction)
{
	if (newAction == ECharacterActionStateEnum::IDLE)
	{
		CurrentActionState = ECharacterActionStateEnum::IDLE;
	}
	else if (newAction == ECharacterActionStateEnum::JUMP)
	{
		CurrentActionState = ECharacterActionStateEnum::JUMP;
	}
	else if (newAction == ECharacterActionStateEnum::INTERACT)
	{
		CurrentActionState = ECharacterActionStateEnum::INTERACT;
	}
	else
	{
		CurrentActionState = ECharacterActionStateEnum::MOVE;
		if (GetVelocity().IsNearlyZero())
		{
			CurrentActionState = ECharacterActionStateEnum::IDLE;
		}
	}

	return false;
}

bool AA2Character::UpdateCrouchState(ECharacterCrouchStateEnum updatedCrouchState)
{
	if (updatedCrouchState == ECharacterCrouchStateEnum::CROUCH)
	{
		IsCrouching = ECharacterCrouchStateEnum::CROUCH;
	}
	else
	{
		IsCrouching = ECharacterCrouchStateEnum::NOTCROUCH;
	}

	return false;
}

bool AA2Character::UpdateRunningState(ECharacterRunningStateEnum updatedRunningState)
{
	if (IsRunning == ECharacterRunningStateEnum::WALK)
	{
		IsRunning = ECharacterRunningStateEnum::RUN;
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, TEXT("Running Mode"));
	}
	else
	{
		IsRunning = ECharacterRunningStateEnum::WALK;
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, TEXT("Walking Mode"));
	}

	return false;
}

// TODO: make interaction
void AA2Character::BeginInteraction()
{

	FHitResult HitResult;

	FCollisionObjectQueryParams DesiredObject;
	DesiredObject.AddObjectTypesToQuery(ECollisionChannel::ECC_Visibility);

	FCollisionQueryParams QueryParams = FCollisionQueryParams::DefaultQueryParam;
	QueryParams.AddIgnoredActor(this->GetOwner());

	DrawDebugLine(GetWorld(), GetActorLocation(), GetActorLocation() + GetActorForwardVector() * 250, FColor(52, 220, 239), true);
	bool hitValidObject = GetWorld()->LineTraceSingleByObjectType(HitResult, GetActorLocation(), GetActorLocation() + GetActorForwardVector() * 250, ECC_WorldDynamic, QueryParams);

	if (hitValidObject)
	{

		if (HitResult.GetActor()->IsA(AA2Pickupable::StaticClass()))
		{
			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, TEXT("Ray Hit Pickupable!"));

			holdingObject = true;

			pickedUpObject = HitResult.GetActor();
			FAttachmentTransformRules AttachmentTransformRules = FAttachmentTransformRules(EAttachmentRule::KeepRelative, true);
			pickedUpObject->AttachToComponent(PickupableAttachPoint, AttachmentTransformRules);
			pickedUpObject->SetActorLocation(PickupableAttachPoint->GetComponentLocation());
			pickedUpObject->FindComponentByClass<UPrimitiveComponent>()->SetSimulatePhysics(false);
		}

		else if (IsCrouching != ECharacterCrouchStateEnum::CROUCH && HitResult.GetActor()->IsA(AA2LeverActor::StaticClass()))
		{
			UpdateAction(ECharacterActionStateEnum::INTERACT);

			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, TEXT("Ray Hit Lever!"));
			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, TEXT("Begin Interaction"));
			leverActor = Cast<AA2LeverActor>(HitResult.GetActor());

			GetWorld()->GetTimerManager().SetTimer(TimerHandle, this, &AA2Character::EndLeverInteraction, 3.0f, false);
		}
	}
}

void AA2Character::DropObject()
{
	if (holdingObject && pickedUpObject)
	{
		pickedUpObject->SetActorEnableCollision(true);
		pickedUpObject->DetachFromActor(FDetachmentTransformRules::KeepRelativeTransform);
		pickedUpObject->SetActorLocation(PickupableAttachPoint->GetComponentLocation());
		pickedUpObject->FindComponentByClass<UPrimitiveComponent>()->SetSimulatePhysics(true);
		holdingObject = false;
	}
}

void AA2Character::EndLeverInteraction()
{
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, TEXT("End Interaction"));
	leverActor->OnLeverDelegate.Broadcast();
	UpdateAction(ECharacterActionStateEnum::IDLE);
}

void AA2Character::BeginCrouch()
{
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, TEXT("Begin Crouching"));
	Crouch();
}

void AA2Character::EndCrouch()
{
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, TEXT("End Crouching"));
	UnCrouch();
}

void AA2Character::SwitchRunningMode()
{
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, TEXT("Running Mode"));
}

// Called when the game starts or when spawned
void AA2Character::BeginPlay()
{
	Super::BeginPlay();
	IsCrouching = ECharacterCrouchStateEnum::NOTCROUCH;
	CurrentActionState = ECharacterActionStateEnum::IDLE;
	SpringArmComp->SetWorldRotation(FRotator(-30.0f, GetControlRotation().Yaw, GetControlRotation().Roll));
}

// Called every frame
void AA2Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bSnappingCharacter)
	{
		SnapCharToRotation();
	}
}

// Called to bind functionality to input
void AA2Character::SetupPlayerInputComponent(UInputComponent *PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

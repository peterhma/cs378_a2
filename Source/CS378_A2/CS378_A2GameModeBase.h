// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CS378_A2GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CS378_A2_API ACS378_A2GameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	ACS378_A2GameModeBase();
};

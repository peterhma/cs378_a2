// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "A2PlayerController.generated.h"

/**
 * 
 */
UCLASS()
class CS378_A2_API AA2PlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AA2PlayerController();

	virtual void SetupInputComponent() override;

protected:
	UFUNCTION()
	void Move(float value);

	UFUNCTION()
	void Strafe(float value);

	UFUNCTION()
	void Jump();

	UFUNCTION()
	void InteractBegan();

	UFUNCTION()
	void InteractEnded();

	UFUNCTION()
	void PitchCamera(float AxisValue);

	UFUNCTION()
	void YawCamera(float AxisValue);

	UFUNCTION()
	void StartSnapCharacter();

	UFUNCTION()
	void StopSnapCharacter();

	UFUNCTION()
	void CrouchBegan();

	UFUNCTION()
	void CrouchEnded();

	UFUNCTION()
	void RunningModeSwitch();
};

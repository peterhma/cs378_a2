// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "A2ResponseActor.generated.h"

class AA2LeverActor;

UCLASS()
class CS378_A2_API AA2ResponseActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AA2ResponseActor();

	FORCEINLINE class UStaticMeshComponent *GetMeshComponent() const { return MeshComponent; }

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void RespondToLever();

	UPROPERTY(Category = Mesh, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent *MeshComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AA2LeverActor *A2LeverActor;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};

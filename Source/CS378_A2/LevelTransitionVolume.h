// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "LevelTransitionVolume.generated.h"

/**
 * 
 */
UCLASS()
class CS378_A2_API ALevelTransitionVolume : public ATriggerBox
{
	GENERATED_BODY()

protected:
	UFUNCTION()
		void OnOverlap(AActor* OverlappedActor, AActor* OtherActor);

	virtual void BeginPlay() override;

	UFUNCTION()
		void SwapLevel();

};

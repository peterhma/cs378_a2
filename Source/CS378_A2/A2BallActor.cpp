// Fill out your copyright notice in the Description page of Project Settings.


#include "A2BallActor.h"

// Sets default values
AA2BallActor::AA2BallActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	RootComponent = MeshComponent;
}

// Called when the game starts or when spawned
void AA2BallActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AA2BallActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


// Fill out your copyright notice in the Description page of Project Settings.


#include "A2Pickupable.h"
#include "Components/SphereComponent.h"

// Sets default values
AA2Pickupable::AA2Pickupable()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	RootComponent = MeshComponent;

	TriggerComponent = CreateDefaultSubobject<USphereComponent>(TEXT("TriggerComponent"));
	TriggerComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
}

// Called when the game starts or when spawned
void AA2Pickupable::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AA2Pickupable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


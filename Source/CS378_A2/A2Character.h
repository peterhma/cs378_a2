// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "A2LeverActor.h"
#include "A2Character.generated.h"

UENUM(BlueprintType)
enum class ECharacterActionStateEnum : uint8
{
	IDLE UMETA(DisplayName = "Idling"),
	MOVE UMETA(DisplayName = "Moving"),
	JUMP UMETA(DisplayName = "Jumping"),
	INTERACT UMETA(DisplayName = "Interacting")
};

UENUM(BlueprintType)
enum class ECharacterCrouchStateEnum : uint8
{
	CROUCH UMETA(DisplayName = "Crouching"),
	NOTCROUCH UMETA(DisplayName = "NotCrouching")
};

UENUM(BlueprintType)
enum class ECharacterRunningStateEnum : uint8
{
	WALK UMETA(DisplayName = "Walking"),
	RUN UMETA(DisplayName = "Running")

};

UCLASS()
class CS378_A2_API AA2Character : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AA2Character();
	UFUNCTION(BlueprintImplementableEvent)
	void Move(float value);

	UFUNCTION(BlueprintImplementableEvent)
	void Strafe(float value);

	UFUNCTION(BlueprintImplementableEvent)
	void InteractionStarted();

	UFUNCTION(BlueprintImplementableEvent)
	void InteractionEnded();

	UFUNCTION(BlueprintImplementableEvent)
	void JumpStarted();

	UFUNCTION(BlueprintImplementableEvent)
	void PitchCamera(float value);

	UFUNCTION(BlueprintImplementableEvent)
	void YawCamera(float value);

	UFUNCTION(BlueprintImplementableEvent)
	void SnapCharacter();

	UFUNCTION(BlueprintImplementableEvent)
	void CrouchBegan();

	UFUNCTION(BlueprintImplementableEvent)
	void CrouchEnded();

	UFUNCTION(BlueprintImplementableEvent)
	void RunningModeSwitch();

	UFUNCTION(BlueprintImplementableEvent)
	void StartCharSnapping();

	UFUNCTION(BlueprintImplementableEvent)
	void StopCharSnapping();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	ECharacterActionStateEnum CurrentActionState;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	ECharacterCrouchStateEnum IsCrouching;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	ECharacterRunningStateEnum IsRunning;

	UFUNCTION(BlueprintCallable)
	bool UpdateRunningState(ECharacterRunningStateEnum updatedRunningState);

	UFUNCTION(BlueprintCallable)
	bool UpdateCrouchState(ECharacterCrouchStateEnum updatedCrouchState);

	UFUNCTION(BlueprintCallable)
	bool CanPerformAction(ECharacterActionStateEnum updatedAction);
	UFUNCTION(BlueprintCallable)
	bool UpdateAction(ECharacterActionStateEnum newAction);

	UFUNCTION(BlueprintCallable)
	void ApplyMovement(float value);
	UFUNCTION(BlueprintCallable)
	void ApplyStrafe(float value);

	UFUNCTION(BlueprintCallable)
	void BeginInteraction();
	UFUNCTION(BlueprintCallable)
	void DropObject();
	UFUNCTION(BlueprintCallable)
	void EndLeverInteraction();

	UFUNCTION(BlueprintCallable)
	void ApplyPitch(float value);

	UFUNCTION(BlueprintCallable)
	void ApplyYaw(float value);

	UFUNCTION(BlueprintCallable)
	void BeginCrouch();
	UFUNCTION(BlueprintCallable)
	void EndCrouch();

	UFUNCTION(BlueprintCallable)
	void SwitchRunningMode();

	UFUNCTION(BlueprintCallable)
	void SnapCharToRotation();

	UFUNCTION(BlueprintCallable)
	void StartSnappingRotation();

	UFUNCTION(BlueprintCallable)
	void StopSnappingRotation();

	UPROPERTY(EditAnywhere)
	class USpringArmComponent *SpringArmComp;

	UPROPERTY(EditAnywhere)
	class UCameraComponent *CameraComp;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent *StaticMeshComp;

	UPROPERTY(EditAnywhere)
	USceneComponent *PickupableAttachPoint;

	bool bSnappingCharacter;

	FVector2D MovementInput;
	FVector2D CameraInput;

	bool holdingObject;
	AActor *pickedUpObject;

	AA2LeverActor *leverActor;

public:
	// Called every frame
	virtual void
	Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent *PlayerInputComponent) override;

	FTimerHandle TimerHandle;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "A2Pickupable.generated.h"

UCLASS()
class CS378_A2_API AA2Pickupable : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AA2Pickupable();

	FORCEINLINE class UStaticMeshComponent* GetMeshComponent() const { return MeshComponent; }
	FORCEINLINE class USphereComponent* GetTriggerComponent() const { return TriggerComponent; }

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/* The mesh component */
	UPROPERTY(Category = Mesh, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* MeshComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		USphereComponent* TriggerComponent;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};

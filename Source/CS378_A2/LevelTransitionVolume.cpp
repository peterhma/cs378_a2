// Fill out your copyright notice in the Description page of Project Settings.


#include "LevelTransitionVolume.h"
#include "A2Character.h"
#include "Kismet/GameplayStatics.h"

void ALevelTransitionVolume::BeginPlay() {
	OnActorBeginOverlap.AddDynamic(this, &ALevelTransitionVolume::OnOverlap);
}

void ALevelTransitionVolume::SwapLevel()
{
	//UGameplayStatics::OpenLevel(GetWorld(), "Arena2");
	FString LevelName = GetWorld()->GetMapName();
	LevelName.RemoveFromStart(GetWorld()->StreamingLevelsPrefix);
	if (LevelName.Equals("Arena1")) {
		UGameplayStatics::OpenLevel(GetWorld(), "Arena2");
	}
	else {
		UGameplayStatics::OpenLevel(GetWorld(), "Arena1");
	}
	
}

void ALevelTransitionVolume::OnOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	if (OtherActor->IsA(AA2Character::StaticClass())) {
		SwapLevel();
	}
}

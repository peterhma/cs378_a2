// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "A2BallActor.generated.h"

UCLASS()
class CS378_A2_API AA2BallActor : public AActor
{
	GENERATED_BODY()
	FORCEINLINE class UStaticMeshComponent *GetMeshComponent() const { return MeshComponent; }

public:	
	// Sets default values for this actor's properties
	AA2BallActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(Category = Mesh, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent *MeshComponent;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};

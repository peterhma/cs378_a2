// Fill out your copyright notice in the Description page of Project Settings.

#include "A2LeverActor.h"
#include "Components/SphereComponent.h"

// Sets default values
AA2LeverActor::AA2LeverActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	RootComponent = MeshComponent;

	HitComponent = CreateDefaultSubobject<USphereComponent>(TEXT("HitComponent"));
	HitComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
}

// Called when the game starts or when spawned
void AA2LeverActor::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AA2LeverActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Copyright Epic Games, Inc. All Rights Reserved.

#include "CS378_A2GameModeBase.h"
#include "A2PlayerController.h"
#include "A2Character.h"

ACS378_A2GameModeBase::ACS378_A2GameModeBase()
{
    PlayerControllerClass = AA2PlayerController::StaticClass();

    static ConstructorHelpers::FObjectFinder<UClass> pawnBPClass(TEXT("Class'/Game/Blueprint/A2CharacterBP.A2CharacterBP_C'"));
    if (pawnBPClass.Object)
    {
        UClass *pawnBP = (UClass *)pawnBPClass.Object;
        DefaultPawnClass = pawnBP;
    }
    else
    {
        DefaultPawnClass = AA2Character::StaticClass();
    }
}
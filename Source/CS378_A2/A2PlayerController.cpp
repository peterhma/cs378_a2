// Fill out your copyright notice in the Description page of Project Settings.

#include "A2PlayerController.h"
#include "A2Character.h"

AA2PlayerController::AA2PlayerController()
{
    //AutoPossessPlayer = EAutoReceiveInput::Player0;
}
void AA2PlayerController::SetupInputComponent()
{
    Super::SetupInputComponent();

    InputComponent->BindAxis("Move", this, &AA2PlayerController::Move);
    InputComponent->BindAxis("Strafe", this, &AA2PlayerController::Strafe);
    InputComponent->BindAction("Jump", IE_Pressed, this, &AA2PlayerController::Jump);
    InputComponent->BindAxis("CameraPitch", this, &AA2PlayerController::PitchCamera);
    InputComponent->BindAxis("CameraYaw", this, &AA2PlayerController::YawCamera);
    InputComponent->BindAction("RunningModeSwitch", IE_Pressed, this, &AA2PlayerController::RunningModeSwitch);

    InputComponent->BindAction("SnapCharacter", IE_Pressed, this, &AA2PlayerController::StartSnapCharacter);
    InputComponent->BindAction("SnapCharacter", IE_Released, this, &AA2PlayerController::StopSnapCharacter);
    InputComponent->BindAction("Interact", IE_Pressed, this, &AA2PlayerController::InteractBegan);
    InputComponent->BindAction("Interact", IE_Released, this, &AA2PlayerController::InteractEnded);

    InputComponent->BindAction("Crouch", IE_Pressed, this, &AA2PlayerController::CrouchBegan);
    InputComponent->BindAction("Crouch", IE_Released, this, &AA2PlayerController::CrouchEnded);
}

void AA2PlayerController::Move(float value)
{
    AA2Character *character = Cast<AA2Character>(this->GetCharacter());
    if (character)
    {
        character->Move(value);
    }
}

void AA2PlayerController::Strafe(float value)
{
    AA2Character *character = Cast<AA2Character>(this->GetCharacter());
    if (character)
    {
        character->Strafe(value);
    }
}

void AA2PlayerController::Jump()
{
    AA2Character *character = Cast<AA2Character>(this->GetCharacter());
    if (character)
    {
        character->JumpStarted();
    }
}

void AA2PlayerController::InteractBegan()
{
    AA2Character *character = Cast<AA2Character>(this->GetCharacter());
    if (character)
    {
        character->InteractionStarted();
    }
}

void AA2PlayerController::InteractEnded()
{
    AA2Character *character = Cast<AA2Character>(this->GetCharacter());
    if (character)
    {
        character->InteractionEnded();
    }
}

void AA2PlayerController::PitchCamera(float AxisValue)
{
    AA2Character *character = Cast<AA2Character>(this->GetCharacter());
    if (character)
    {
        character->PitchCamera(AxisValue);
    }
}

void AA2PlayerController::YawCamera(float AxisValue)
{
    AA2Character *character = Cast<AA2Character>(this->GetCharacter());
    if (character)
    {
        character->YawCamera(AxisValue);
    }
}

void AA2PlayerController::StartSnapCharacter()
{
    AA2Character *character = Cast<AA2Character>(this->GetCharacter());
    if (character)
    {
        character->StartCharSnapping();
    }
}

void AA2PlayerController::StopSnapCharacter()
{
    AA2Character* character = Cast<AA2Character>(this->GetCharacter());
    if (character)
    {
        character->StopCharSnapping();
    }
}

void AA2PlayerController::CrouchBegan()
{
    AA2Character *character = Cast<AA2Character>(this->GetCharacter());
    if (character)
    {
        character->CrouchBegan();
    }
}

void AA2PlayerController::CrouchEnded()
{
    AA2Character *character = Cast<AA2Character>(this->GetCharacter());
    if (character)
    {
        character->CrouchEnded();
    }
}

void AA2PlayerController::RunningModeSwitch()
{
    AA2Character *character = Cast<AA2Character>(this->GetCharacter());
    if (character)
    {
        character->RunningModeSwitch();
    }
}
